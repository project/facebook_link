
Facebook-style Links (FBSL) provides user the ability to attach links with their Facebook-style Statuses (FBSS). FBSL adds form elements to the existing FBSS form. The structure of the appended form is available in the file "structure.txt" and can be themed accordingly. 


Visit the project page at: http://drupal.org/project/facebook_link

Dependencies:
-------------
Facebook-style Statuses (FBSS);

Installation:
-------------
1) Place this module directory in your modules folder (this will usually be
"sites/all/modules/").

2) Download the UrlToAbsolute package (http://sourceforge.net/projects/absoluteurl) and copy the "url_to_absolute.php" file to the facebook_link directory.

3) Enable the module.

Sponsors:
---------
This module has been sponsored by Thomas Cermak of LondonFuse (http://londonfuse.ca).

FAQ
-----

1) How can I change the themeing of the FBSL form?
You can look at the structure of the FBSL form at various times during the attachment process in the structure.txt file and then edit images/facebook_link.css file to have a look of your choice.

Author:
--------
publicmind wrote and maintains this module. The author can be contacted for paid customizations of this module as well as
Drupal consulting and development at http://drupal.org/user/472412