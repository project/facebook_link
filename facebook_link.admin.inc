<?php

/**
 * @file
 *   Handles admin pages for the Facebook-style Links module.
 */

/**
 * Settings form.
 */
function facebook_link_admin($form_state) {

  $form['facebook_link_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Field size of Title and Description fields'),
    '#description' => t('The width of the title and description textfield in the block.'),
    '#default_value' => variable_get('facebook_link_size', 40),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['facebook_link_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum title and description length'),
    '#description' => t('Must be a positive integer no greater than 21844.'),
    '#default_value' => variable_get('facebook_link_length', 250),
    '#size' => 3,
    '#maxlength' => 5,
    '#required' => TRUE,
  );

  $form['facebook_link_maximum_image_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum size of the image(in KB)'),
    '#description' => t('Must be a positive integer no greater than 1024.'),
    '#default_value' => variable_get('facebook_link_maximum_image_size', 500),
    '#size' => 3,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['facebook_link_image_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width of thumbnail image shown in the status'),
    '#description' => t('Must be a positive integer no greater than 1024.'),
    '#default_value' => variable_get('facebook_link_image_width', 100),
    '#size' => 3,
    '#maxlength' => 4,
    '#required' => TRUE,
  );

  $max_exec = ini_get('max_execution_time') == 0 ? 30 : ini_get('max_execution_time');
  $form['facebook_link_time_percentage'] = array(
    '#type' => 'select',
    '#title' => t('Maximum time for link attachment[%]'),
    '#options' => drupal_map_assoc(array(15, 25, 50, 75)),
    '#default_value' => variable_get('facebook_link_time_percentage', 75),
    '#description' => t('Percentage of maximal PHP execution time (currently !exec seconds) to take while attaching the link.', array("!exec" => $max_exec)),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validate function for the settings form.
 */
function facebook_link_admin_validate($form, &$form_state) {
  $size = $form_state['values']['facebook_link_size'];
  if (!is_numeric($size) || $size < 1 || $size != round($size)) {
    form_set_error('facebook_link_size', t('The size of the status update field must be a positive integer.'));
  }
  //21844 is 65535 / 3 - 1. In MySQL, 65535 is the maximum number of bytes a
  //text column can hold, and in UTF8 each character takes 3 bytes of storage.
  $size = $form_state['values']['facebook_link_length'];
  if (!is_numeric($size) || $size < 1 || $size != round($size) || $size > 21844) {
    form_set_error('facebook_link_length', t('The maximum status length must be a positive integer no greater than 21844.'));
  }
  $size = $form_state['values']['facebook_link_maximum_image_size'];
  if (!is_numeric($size) || $size < 1 || $size != round($size) || $size > 1024) {
    form_set_error('facebook_link_maximum_image_size', t('The maximum image size must be a positive integer no greater than 1024.'));
  }
  $size = $form_state['values']['facebook_link_image_width'];
  if (!is_numeric($size) || $size < 1 || $size != round($size) || $size > 1024) {
    form_set_error('facebook_link_image_width', t('The thumbnail width must be a positive integer no greater than 1024.'));
  }
  $size = $form_state['values']['facebook_link_time_percentage'];
  if (!is_numeric($size) || $size < 15 || $size != round($size) || $size > 75) {
    form_set_error('facebook_link_time_percentage', t('Select the correct option.'));
  }
}

