Drupal.behaviors.facebookLink = function (context) {
    
    $facebook_link_prev_button = $('#facebook-link-prevbutton');
    $facebook_link_next_button = $('#facebook-link-nextbutton');
    $facebook_link_max_count = $('#edit-facebook-link-share-link-image-count');
    $facebook_link_current_count = $('#edit-facebook-link-share-link-current-image');
    $facebook_link_chars = $('#facebook-link-chars');
    $facebook_link_thumbnail = $('#fl-thumbnail');
    
    $facebook_link_prev_button.click(function() {
        var current = $facebook_link_current_count.val();
        var max = $facebook_link_max_count.val();
        current--;
        if(current>0){
            $facebook_link_current_count.val(current);
            facebook_link_print_count(current,max);
            facebook_link_change_image(current);
        }
        
        
        
        
    } );
    
    $facebook_link_next_button.click(function() {
        var current = $facebook_link_current_count.val();
        var max = $facebook_link_max_count.val();
        current++;
        if(current<=max){
            $facebook_link_current_count.val(current);
            facebook_link_print_count(current,max);
            facebook_link_change_image(current);
        }
        
    } );
    
}


function facebook_link_print_count(current, max) {
  var facebook_link_translations = new Array();
  facebook_link_translations['%current'] = current;
  facebook_link_translations['%max'] = max;
  $facebook_link_chars.html(Drupal.t('%current of %max', facebook_link_translations));

}

function facebook_link_change_image(current) {
    var change = "#edit-facebook-link-share-link-thumb-" + current ;
    $facebook_link_change = $(change);
    var new_thumb = $facebook_link_change.val();
    $facebook_link_thumbnail.attr('src',new_thumb);
}

